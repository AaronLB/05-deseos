import { Injectable } from '@angular/core';
import { List } from '../models/list.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  lists: List []=[];

  constructor() {
    
    this.loadStorage();
    // const list1 = new List('Recolectar priedras del infinito');
    // const list2 = new List('Heroes a desaparecer');

    // this.lists.push(list1,list2);
    // console.log(this.lists);
    
  }

  createList ( title: string ){
    const newList = new List(title);
    this.lists.push(newList);
    this.saveStorage();
    return newList.id;
  }

  deleteList( list:List ){
    this.lists = this.lists.filter( dataList => dataList.id != list.id );
    this.saveStorage();
  }

  getList( id:string | number ){
    id = Number(id);
    return this.lists.find( dataList => dataList.id === id );
  }

  saveStorage(){
    localStorage.setItem('data',JSON.stringify(this.lists));
  }

  loadStorage(){

    if (localStorage.getItem('data')) {
      
      this.lists = JSON.parse( localStorage.getItem('data') || '{}' );
    }
    

  }
}
