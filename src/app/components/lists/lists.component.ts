import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { List } from 'src/app/models/list.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss'],
})
export class ListsComponent  implements OnInit {

  @Input() finished = true;

  constructor(
    public deseosServices: DeseosService,
    private router: Router,
    private alertCtrl: AlertController
  ) { }

  selectList( list:List ){
    if (this.finished) {
      this.router.navigateByUrl(`/tabs/tab2/add/${list.id}`);
    }
    else {
      this.router.navigateByUrl(`/tabs/tab1/add/${list.id}`);

    }
  }

  deleteList( List:List ){
    this.deseosServices.deleteList(List);
    this.presentAlert();
  }

  
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'The list was successfully deleted ',
      // message: 'This is an alert!',
      buttons: ['OK'],
    });

    alert.present();
  }
  ngOnInit() {}

}
