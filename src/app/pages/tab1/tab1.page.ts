import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor( public deseosServices: DeseosService,
               private router: Router,
               private alertCtrl: AlertController
               ) {}

  async addList(){
    // this.router.navigateByUrl('/tabs/tab1/add');
    const alert = await this.alertCtrl.create({
      header: 'New list',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'List name'
        }
      ],
      buttons: [
        {
          text:'Cancel',
          role:'cancel',
          handler: ()=>{
            console.log('cancel');
            
          }
        },
        {
          text:'Create',
          handler: ( data ) =>{
            console.log(data);
            if (data.title.length === 0) {
              return;
            }
            // create list
            const id = this.deseosServices.createList( data.title );

            this.router.navigateByUrl(`/tabs/tab1/add/${id}`);
          }
        }
      ],
    });
    alert.present();
    // await alert.present();
  }

  
}
