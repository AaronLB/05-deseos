import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { List } from '../../models/list.model';
import { DeseosService } from 'src/app/services/deseos.service';
import { ListItem } from 'src/app/models/list-item.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  List?: List;
  nameItem = '';

  constructor( private deseosServices: DeseosService,
               private route: ActivatedRoute) { 
    const listaId = this.route.snapshot.paramMap.get('listaId');
    console.log(listaId);
    if (listaId !== null ) {
      
      this.List = this.deseosServices.getList(listaId);
      console.log(this.List);
    }
    
  }

  ngOnInit() {
  }

  addItems(){
    if (this.nameItem.length === 0) {
      return;
    }

    const newItem = new ListItem( this.nameItem) ;
    this.List?.items.push(newItem);
    this.nameItem = '';
    this.deseosServices.saveStorage();
  }

  updateCheck( item:ListItem ){

    const pending = this.List?.items.filter( itemData =>!itemData.complit).length;
    console.log({pending});
    
    if ( pending === 0 ) {
      this.List!.endedAt = new Date();
      this.List!.finished = true;
    } else {
      // this.List!.endedAt = null;
      this.List!.finished = false;
      
    }
    
    this.deseosServices.saveStorage();

    console.log(this.deseosServices.lists);
    
    
  }

  deleteItem( i:number ){
    
    this.List?.items.splice(i, 1);
    this.deseosServices.saveStorage();

  }
}
